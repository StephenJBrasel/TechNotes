code --install-extension 4ops.terraform
code --install-extension aaron-bond.better-comments
code --install-extension abusaidm.html-snippets
code --install-extension acidic9.p5js-snippets
code --install-extension adashen.vscode-tomcat
code --install-extension alefragnani.Bookmarks
code --install-extension amazonwebservices.aws-toolkit-vscode
code --install-extension anseki.vscode-color
code --install-extension basarat.god
code --install-extension batisteo.vscode-django
code --install-extension byCedric.vscode-expo
code --install-extension christian-kohler.npm-intellisense
code --install-extension chrmarti.regex
code --install-extension chunsen.bracket-select
code --install-extension codezombiech.gitignore
code --install-extension CoenraadS.bracket-pair-colorizer-2
code --install-extension dbaeumer.vscode-eslint
code --install-extension Denis-net.vscode-ahk-manager
code --install-extension donjayamanne.githistory
code --install-extension donjayamanne.python-extension-pack
code --install-extension dsznajder.es7-react-js-snippets
code --install-extension eamodio.gitlens
code --install-extension ecmel.vscode-html-css
code --install-extension eg2.vscode-npm-script
code --install-extension elmTooling.elm-ls-vscode
code --install-extension emilylilylime.vscode-test-explorer-diagnostics
code --install-extension EQuimper.react-native-react-redux
code --install-extension esbenp.prettier-vscode
code --install-extension filipesabella.live-p5
code --install-extension formulahendry.auto-close-tag
code --install-extension formulahendry.auto-rename-tag
code --install-extension formulahendry.code-runner
code --install-extension formulahendry.terminal
code --install-extension garrit.p5canvas
code --install-extension geyao.html-snippets
code --install-extension GitHub.remotehub
code --install-extension golang.go
code --install-extension GrapeCity.gc-excelviewer
code --install-extension hashicorp.terraform
code --install-extension hbenl.vscode-test-explorer
code --install-extension hbenl.vscode-test-explorer-liveshare
code --install-extension hoovercj.vscode-power-mode
code --install-extension hoovercj.vscode-settings-cycler
code --install-extension ionutvmi.path-autocomplete
code --install-extension JakeWilson.vscode-picture
code --install-extension jock.svg
code --install-extension kameshkotwani.google-search
code --install-extension Kasik96.swift
code --install-extension kavod-io.vscode-jest-test-adapter
code --install-extension l7ssha.tag-inserter
code --install-extension lolkush.quickstart
code --install-extension LuqueDaniel.languague-renpy
code --install-extension magicstack.MagicPython
code --install-extension marchiore.csvtomarkdown
code --install-extension mechatroner.rainbow-csv
code --install-extension moshfeu.diff-merge
code --install-extension ms-azuretools.vscode-docker
code --install-extension ms-dotnettools.csharp
code --install-extension ms-python.python
code --install-extension ms-python.vscode-pylance
code --install-extension ms-toolsai.jupyter
code --install-extension ms-vscode-remote.remote-wsl
code --install-extension ms-vscode.azure-account
code --install-extension ms-vscode.cpptools
code --install-extension ms-vscode.test-adapter-converter
code --install-extension ms-vsliveshare.vsliveshare
code --install-extension msjsdiag.debugger-for-chrome
code --install-extension msjsdiag.vscode-react-native
code --install-extension naumovs.color-highlight
code --install-extension oderwat.indent-rainbow
code --install-extension oouo-diogo-perdigao.docthis
code --install-extension Orta.vscode-jest
code --install-extension patbenatar.advanced-new-file
code --install-extension pnp.polacode
code --install-extension pranaygp.vscode-css-peek
code --install-extension quicktype.quicktype
code --install-extension rajeshroyal896.extended-html5-boilerplate
code --install-extension RandomFractalsInc.vscode-data-preview
code --install-extension rangav.vscode-thunder-client
code --install-extension rebornix.toggle
code --install-extension redhat.java
code --install-extension redhat.vscode-commons
code --install-extension redhat.vscode-yaml
code --install-extension ritwickdey.LiveServer
code --install-extension shd101wyy.markdown-preview-enhanced
code --install-extension sidthesloth.html5-boilerplate
code --install-extension sleistner.vscode-fileutils
code --install-extension slevesque.vscode-autohotkey
code --install-extension snyk-security.vscode-vuln-cost
code --install-extension streetsidesoftware.code-spell-checker
code --install-extension TabNine.tabnine-vscode
code --install-extension TaodongWu.ejs-snippets
code --install-extension techer.open-in-browser
code --install-extension theumletteam.umlet
code --install-extension Tobiah.language-pde
code --install-extension tomoki1207.pdf
code --install-extension Unity.unity-debug
code --install-extension vinnyjames.vscode-autohotkey-vj
code --install-extension VisualStudioExptTeam.vscodeintellicode
code --install-extension vscjava.vscode-java-debug
code --install-extension vscjava.vscode-java-dependency
code --install-extension vscjava.vscode-java-pack
code --install-extension vscjava.vscode-java-test
code --install-extension vscjava.vscode-maven
code --install-extension vscode-icons-team.vscode-icons
code --install-extension walkme.HTML5-extension-pack
code --install-extension WallabyJs.quokka-vscode
code --install-extension wayou.vscode-todo-highlight
code --install-extension wix.vscode-import-cost
code --install-extension wmaurer.change-case
code --install-extension xshrim.txt-syntax
code --install-extension yzhang.markdown-all-in-one
code --install-extension Zignd.html-css-class-completion
